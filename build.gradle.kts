plugins {
    `java-gradle-plugin`
    `kotlin-dsl`
    `maven-publish`
    // Apply the Plugin Publish Plugin to make plugin publication possible
    id("com.gradle.plugin-publish") version "0.20.0"
    kotlin("jvm") version "1.5.10"
}

group = "tw.thinkyes"
version = "0.0.13"

repositories {
    mavenLocal()
    mavenCentral()
    google()
}

pluginBundle {
    website = "https://gitlab.com/gradle-plugin1/kmm-routine-plugin"
    vcsUrl = "https://gitlab.com/gradle-plugin1/kmm-routine-plugin"
    description = "KMM routine plugin"
    tags = listOf("KMM", "kotlin multiplatform")
}

gradlePlugin {
    plugins {
        create("kmmRoutinePlugin") {
            id = "tw.thinkyes.kmm.routine.plugin"
            displayName = "KMM Routine Plugin"
            implementationClass = "tw.thinkyes.kmm.routine.plugin.KmmRoutinePlugin"
        }
    }
}

publishing {
    publications {
    }
    repositories {
        maven {
            setUrl("$projectDir/repository/")
        }
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("com.android.tools.build:gradle:7.0.2")
}

tasks.withType<Copy> {
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}
