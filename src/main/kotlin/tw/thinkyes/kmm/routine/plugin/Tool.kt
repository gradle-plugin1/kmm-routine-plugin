package tw.thinkyes.kmm.routine.plugin

import org.gradle.api.Project
import org.gradle.api.tasks.TaskContainer


class Tool {
    companion object {

        fun logMessage(project: Project, message: String){
            project.logger.warn("> Task :${project.name}:KmmRoutinePlugin:$message")
        }

        fun logMessageError(project: Project, message: String){
            project.logger.error("\n--------------${project.name}:KmmRoutinePlugin: ERROR------------------")
            project.logger.error("> Task :${project.name}:KmmRoutinePlugin:$message")
            project.logger.error("--------------------------------------------------------------------\n")
        }

        fun getXCFrameworkAssembleTask(tasks: TaskContainer): String{
            var dependsTask = ""
            tasks.forEach {
                if(it.name.contains("assemble") && it.name.contains("DebugXCFramework")){
                    dependsTask = it.name
                }
            }
            return dependsTask
        }
    }
}