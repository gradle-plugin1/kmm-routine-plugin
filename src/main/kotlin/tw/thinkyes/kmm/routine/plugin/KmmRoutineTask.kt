package tw.thinkyes.kmm.routine.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.register
import tw.thinkyes.kmm.routine.plugin.Tool.Companion.logMessage
import tw.thinkyes.kmm.routine.plugin.Tool.Companion.logMessageError
import java.io.File

class KmmRoutineTask ( private val project: Project, private val extension: KmmRoutinePluginExtension ) {

    val logTag = "KmmRoutineTask"

    fun createAllTask(){
        project.tasks.register<XCFrameworkAutoAssembleTask>("xcFrameworkAutoAssembleTask") {
            val pluginConfig = extension.genConfigData()
            this.pluginConfig.set(pluginConfig)

            if(!pluginConfig.xcFrameworkAutoAssembleEnable){
                logMessage(project, "$logTag :xcFrameworkAutoAssembleTask false, SKIP")
                return@register
            }

            val dependsTask = Tool.getXCFrameworkAssembleTask(project.tasks)
            if(dependsTask.isNotEmpty()){
                logMessage(project, "$logTag:DependsTask:$dependsTask")
                dependsOn(dependsTask)
            } else {
                logMessage(project, "$logTag:DependsTask not found, skip")
            }
        }
        project.tasks.getByName("preBuild").dependsOn("xcFrameworkAutoAssembleTask")

        project.tasks.register<XCFrameworkOutputCopy>("xcFrameworkOutputCopy") {
            val pluginConfig = extension.genConfigData()
            this.pluginConfig.set(pluginConfig)
            logMessage(project, "$logTag :xcFrameworkOutputCopy ${pluginConfig.xcFrameworkOutputCopyEnable}")
        }

        project.tasks.register<CleanGitStageTask>("cleanGitStage") {
            logMessage(project, "$logTag:setup")
            this.pluginConfig.set(extension.genConfigData())
        }

        project.tasks.named("preBuild") {
            logMessage(project, "$logTag finalizedBy xcFrameworkOutputCopy")
            finalizedBy("xcFrameworkOutputCopy")
        }

        project.tasks.named("preBuild") {
            logMessage(project, "$logTag finalizedBy cleanGitStage")
            finalizedBy("cleanGitStage")
        }
    }
}

/**
 * 自動執行XCFramework assemble task，主要由preBuild觸發。
 */
abstract class XCFrameworkAutoAssembleTask: DefaultTask() {

    private val logTitle = "XCFrameworkAutoAssembleTask"

    @get:Input
    abstract val pluginConfig: Property<KmmRoutinePluginConfig>

    @TaskAction
    fun assembleTask() {
        if(pluginConfig.get().xcFrameworkAutoAssembleEnable){
            logMessage(project,"$logTitle TaskAction job finished")
        } else {
            //will be call, but do nothing
        }
    }
}

/**
 * XCFramework assemble task 完成後複製一份到指定資料夾，以做為CocoaPods library。
 */
abstract class XCFrameworkOutputCopy: DefaultTask() {

    private val logTitle = "XCFrameworkOutputCopy"

    private val xcFrameworksBuildPrefixPath = "build/XCFrameworks/debug/"
    private val xcFrameworksOutputPrefixPath = "XCFramework/"

    @get:Input
    abstract val pluginConfig: Property<KmmRoutinePluginConfig>

    @TaskAction
    fun copyTask() {
        if(pluginConfig.get().xcFrameworkOutputCopyEnable.not()){
            logMessage(project,"$logTitle:DISABLE:SKIP")
            return
        } else if(pluginConfig.get().lastActionTask() != LastActionTask.XCFrameworkOutputCopy) {
            logMessage(project,"$logTitle:NOT LAST ACTION(${pluginConfig.get().lastActionTask()}):SKIP")
            return
        }
        logMessage(project,"$logTitle:ENABLE:START")

        val dependsTask = Tool.getXCFrameworkAssembleTask(project.tasks)
        if(dependsTask.isNotEmpty()){

            val xcFrameworkDirPath = "${project.rootDir}/${project.name}/$xcFrameworksBuildPrefixPath"
            val xcFrameworkOutputPath = "${project.rootDir}/${project.name}/$xcFrameworksOutputPrefixPath"

            val xcFrameworkDir = File(xcFrameworkDirPath)
            if(xcFrameworkDir.exists().not()){
                logMessageError(project,"$logTitle:XCFrameworksOutput Dir NOT EXIST:SKIP")
                logMessageError(project,"$logTitle:XCFrameworksOutput Path:$xcFrameworkOutputPath")
                return
            }

            val outputDir = File(xcFrameworkOutputPath)
            if(outputDir.exists().not()){
                outputDir.mkdir()
            }
            logMessage(project,"$logTitle :outputDir canWrite  ${outputDir.isDirectory} $outputDir")

            val cleanResult = project.exec {
                workingDir = outputDir
                commandLine = listOf(
                    "cp", "-R",
                    xcFrameworkDirPath,
                    xcFrameworkOutputPath)
            }

            when(cleanResult.exitValue){
                0 -> logMessage(project,"$logTitle :Copy FINISH")
                else -> logMessageError(project,"$logTitle :clean ERROR ${cleanResult.exitValue} ${cleanResult.rethrowFailure()}")
            }
        } else {
            logMessage(project,"$logTitle :DependsTask not found, skip")
        }
    }
}

/**
 * 當非開發狀態時，清除compiler 產生變化以確保Git 不會被影響。
 * target:
 *  -shared_common.podspec
 */
abstract class CleanGitStageTask: DefaultTask() {
    private val logTitleCleanGitStage = "cleanGitStage"

    @get:Input
    abstract val pluginConfig: Property<KmmRoutinePluginConfig>

    @TaskAction
    fun cleanGitStage() {
        logMessage(project,"$logTitleCleanGitStage :cleanGitStage")

        val configData = pluginConfig.get()
        if(configData.cleanGitStageEnable.not()){
            logMessage(project,"$logTitleCleanGitStage DISABLE")
            return
        } else if(pluginConfig.get().lastActionTask() != LastActionTask.CleanGitStage) {
            logMessage(project,"$logTitleCleanGitStage:NOT LAST ACTION(${pluginConfig.get().lastActionTask()}):SKIP")
            return
        }
        logMessage(project,"$logTitleCleanGitStage ENABLE")

        val workDir = File("${project.rootDir}/${project.name}/")
        logMessage(project,"$logTitleCleanGitStage :clean git reset --hard-> $workDir, file-> all")

        val cleanResult = project.exec {
            this.isIgnoreExitValue = true
            workingDir = workDir
            commandLine = listOf(
                "git",
                "reset",
                "--hard")
        }
        when(cleanResult.exitValue){
            0 -> logMessage(project,"$logTitleCleanGitStage :clean FINISH")
            128 -> logMessage(project,"$logTitleCleanGitStage :clean Git not exists")
            else -> logMessage(project,"$logTitleCleanGitStage :clean exitValue ${cleanResult.exitValue}")
        }
    }

}