package tw.thinkyes.kmm.routine.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.register
import javax.inject.Inject

abstract class KmmRoutinePlugin : Plugin<Project> {

    override fun apply(target: Project) {
        print("> Task :${target.name}:KmmRoutinePlugin 1w3f\n")

        val extension = target.extensions.create("kmmRoutinePlugin", KmmRoutinePluginExtension::class.java, target)

        target.afterEvaluate {

            target.tasks.register<TemplateExampleTask>("templateExample"){
                target.logger.warn("${target.name}:KmmRoutinePlugin templateExample block")
            }
            if (target.tasks.findByName("preBuild")  == null){
                target.logger.warn("${target.name}:KmmRoutinePlugin Task preBuild not exist")
            } else {
                target.logger.warn("${target.name}:KmmRoutinePlugin Task preBuild exist")
                target.tasks.getByName("preBuild").dependsOn("templateExample")
            }

            val tasks = KmmRoutineTask(target, extension)
            tasks.createAllTask()
        }
    }

}

abstract class KmmRoutinePluginExtension @Inject constructor(val project: Project)  {

    var moduleName: String? = null
    var xcFrameworkAutoAssemble: Boolean = true
    var xcFrameworkOutputCopy: Boolean = false
    var cleanGitStage: Boolean = false

    fun genConfigData(): KmmRoutinePluginConfig {
        print("> Task :${project.name}:genConfigData value $moduleName $xcFrameworkOutputCopy $cleanGitStage\n")

        val dataModuleName = if(moduleName.isNullOrBlank()){
            ""
        } else {
            moduleName!!
        }

        return KmmRoutinePluginConfig(moduleName = dataModuleName, xcFrameworkAutoAssembleEnable = xcFrameworkAutoAssemble,
            xcFrameworkOutputCopyEnable = xcFrameworkOutputCopy, cleanGitStageEnable = cleanGitStage)
    }
}

data class KmmRoutinePluginConfig (
    var moduleName: String,
    var xcFrameworkAutoAssembleEnable: Boolean = true,
    var xcFrameworkOutputCopyEnable: Boolean = false,
    var cleanGitStageEnable: Boolean = false
){
    fun lastActionTask(): LastActionTask {
        return if(xcFrameworkOutputCopyEnable) {
            LastActionTask.XCFrameworkOutputCopy
        } else if(cleanGitStageEnable) {
            LastActionTask.CleanGitStage
        } else {
            LastActionTask.NONE
        }
    }

}

enum class LastActionTask {
    NONE, XCFrameworkOutputCopy, CleanGitStage
}

abstract class TemplateExampleTask : DefaultTask() {

    init {
        description = "Just a sample template task"

        // Don't forget to set the group here.
        // group = BasePlugin.BUILD_GROUP
    }

    @TaskAction
    fun sampleAction() {
        logger.warn("KmmRoutinePlugin sampleAction")
    }
}